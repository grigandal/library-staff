import tkinter as tk
from tkinter import ttk
import tkinter.simpledialog as sd
import tkinter.filedialog as fd
import os

from utils import *


class MainWindow(tk.Tk):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.main_menu = tk.Menu(self)
        self.configure(menu=self.main_menu)

        file_menu = tk.Menu(self.main_menu, tearoff=0)
        file_menu.add_command(label="Создать...", command=self.create)
        file_menu.add_command(label="Сохранить...", command=self.save)
        file_menu.add_command(label="Открыть...", command=self.open)
        file_menu.add_separator()
        file_menu.add_command(label="Выход", command=self.exit)

        data_menu = tk.Menu(self.main_menu, tearoff=0)
        data_menu.add_command(label='Добавить книгу...')
        data_menu.add_command(label='Удалить книгу...', state=tk.DISABLED)
        data_menu.add_separator()
        data_menu.add_command(label='Добавить пользователя...')
        data_menu.add_command(label='Удалить пользователя...', state=tk.DISABLED)

        self.main_menu.add_cascade(label='Файл', menu=file_menu)
        self.main_menu.add_cascade(label='Данные', menu=data_menu)

        self.tab_control = ttk.Notebook(self)
        self.tab_control.pack(expand=True, fill=tk.BOTH, side=tk.BOTTOM)

        self.book_tab = ttk.Treeview(self.tab_control)
        self.tab_control.add(self.book_tab, text='Книги')

        self.user_tab = ttk.Treeview(self.tab_control)
        self.tab_control.add(self.user_tab, text='Пользователи')

        self.create()

    def load_data(self):
        books = BookControl.all_books()
        users = UserControl.all_users()

    def create(self, *args, **kwargs):
        f = open(os.path.join(Const.data, Const.file), 'w')
        f.write('{"books": [], "users": []}')
        f.close()
        self.load_data()

    def save(self, *args, **kwargs):
        f = fd.asksaveasfile()
        if f is not None:
            f.write(open(os.path.join(Const.data, Const.file)).read())
            f.close()

    def open(self, *args, **kwargs):
        f = fd.askopenfile()
        if f is not None:
            text = f.read()
            d = open(os.path.join(Const.data, Const.file), 'w')
            d.write(text)
            d.close()
            self.load_data()

    def exit(self, *args, **kwargs):
        self.destroy()


if __name__ == '__main__':
    window = MainWindow()
    window.mainloop()
