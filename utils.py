import json
import os


class Const:
    file = 'data.json'
    data = 'data'
    users = 'users'
    books = 'books'


class BookControl:
    @staticmethod
    def all_books():
        if (not os.path.exists(Const.data)) or (not os.path.isdir(Const.data)):
            os.mkdir(Const.data)
        all_books = []
        if os.path.exists(os.path.join(Const.data, Const.file)):
            all_books = json.loads(open(os.path.join(Const.data, Const.file)).read())[Const.books]
        return all_books

    @staticmethod
    def get_book_by_id(id: int):
        all_books = BookControl.all_books()
        for book in all_books:
            if book['id'] == id:
                return book
        return None

    @staticmethod
    def get_free_id():
        id = 0
        while BookControl.get_book_by_id(id) is not None:
            id += 1
        return id

    @staticmethod
    def get_book_by_data(name: str, author: str, publisher: str, theme: str):
        all_books = BookControl.all_books()
        for book in all_books:
            if (book['name'] == name) and (book['author'] == author) and (book['publisher'] == publisher) and (book['theme'] == theme):
                return book
        return None

    @staticmethod
    def realoded_books(books: dict):
        if (not os.path.exists(Const.data)) or (not os.path.isdir(Const.data)):
            os.mkdir(Const.data)
        data = {'books': books, 'users': UserControl.all_users()}
        with open(os.path.join(Const.data, Const.file), 'w') as f:
            f.write(json.dumps(data))
            f.close()

    @staticmethod
    def create_book(name: str, author: str, publisher: str, theme: str, add=False):
        book = BookControl.get_book_by_data(name, author, publisher, theme)
        all_books = BookControl.all_books()
        if book is not None:
            if add:
                index = all_books.index(book)
                all_books[index]['count'] += 1
                open(os.path.join(Const.data, Const.books), 'w').write(json.dumps(all_books))
            else:
                raise ValueError(f'Book "{author}. {name}. {publisher}." already exists')
        book = {
            'id': BookControl.get_free_id(),
            'name': name,
            'author': author,
            'publisher': publisher,
            'theme': theme,
            'count': 0
        }
        all_books = BookControl.all_books()
        all_books.append(book)
        BookControl.realoded_books(all_books)

    @staticmethod
    def delete_book_by_id(id: int):
        if BookControl.get_book_by_id(id) is None:
            raise ValueError(f'Book with id \'{id}\' does not exist')
        all_books = BookControl.all_books()
        index = all_books.index(UserControl.get_user_by_id(id))
        all_books.remove(all_books[index])
        BookControl.realoded_books(all_books)


class UserControl:

    @staticmethod
    def all_users():
        if (not os.path.exists(Const.data)) or (not os.path.isdir(Const.data)):
            os.mkdir(Const.data)
        all_users = []
        if os.path.exists(os.path.join(Const.data, Const.file)):
            all_users = json.loads(open(os.path.join(Const.data, Const.file)).read())[Const.users]
        return all_users

    @staticmethod
    def get_user_by_name(name: str):
        all_users = UserControl.all_users()
        for user in all_users:
            if user['name'] == name:
                return user
        return None

    @staticmethod
    def get_user_by_id(id: int):
        all_users = UserControl.all_users()
        for user in all_users:
            if user['id'] == id:
                return user
        return None

    @staticmethod
    def get_free_id():
        id = 0
        while UserControl.get_user_by_id(id) is not None:
            id += 1
        return id

    @staticmethod
    def realoded_users(users: dict):
        if (not os.path.exists(Const.data)) or (not os.path.isdir(Const.data)):
            os.mkdir(Const.data)
        data = {'books': BookControl.all_books(), 'users': users}
        with open(os.path.join(Const.data, Const.file), 'w') as f:
            f.write(json.dumps(data))
            f.close()

    @staticmethod
    def create_user(name: str):
        if UserControl.get_user_by_name(name) is not None:
            raise ValueError(f'User with name \'{name}\' already exists')
        user = {
            'id': UserControl.get_free_id(),
            'name': name,
            'grads': []
        }
        all_users = UserControl.all_users()
        all_users.append(user)
        UserControl.realoded_users(all_users)

    @staticmethod
    def delete_user_by_id(id: int):
        if UserControl.get_user_by_id(id) is None:
            raise ValueError(f'User with id \'{id}\' does not exist')
        index = UserControl.all_users().index(UserControl.get_user_by_id(id))
        all_users = UserControl.all_users()
        all_users.remove(all_users[index])
        UserControl.realoded_users(all_users)

    @staticmethod
    def delete_user_by_name(name: str):
        if UserControl.get_user_by_name(name) is None:
            raise ValueError(f'User with id \'{id}\' does not exists')
        index = UserControl.all_users().index(UserControl.get_user_by_name(name))
        all_users = UserControl.all_users()
        all_users.remove(all_users[index])
        UserControl.realoded_users(all_users)

    @staticmethod
    def has_grad(user_id, book_id):
        user = UserControl.get_user_by_id(user_id)
        for g in user['grads']:
            if g['book_id'] == book_id:
                return True
        return False

    @staticmethod
    def del_grad(user_id, book_id):
        user = UserControl.get_user_by_id(user_id)
        all_users = UserControl.all_users()
        index = all_users.index(user)
        all_users.remove(user)
        gs = user['grads']
        for g in gs:
            if g['book_id'] == book_id:
                gs.remove(g)
                break

        user['grads'] = gs
        all_users.insert(index, user)
        UserControl.realoded_users(all_users)

    @staticmethod
    def set_grad(user_id: int, book_id: int, grad: int):
        grad = int(grad)
        if grad not in range(5):
            raise ValueError(f'Expected graduation value in range [0; 5] but got {grad}')
        if BookControl.get_book_by_id(book_id) is None:
            raise ValueError(f'Book with id \'{id}\' does not exist')
        UserControl.del_grad(user_id, book_id)

        g = {
            'book_id': book_id,
            'grad': grad
        }
        user = UserControl.get_user_by_id(user_id)
        all_users = UserControl.all_users()
        index = all_users.index(user)
        all_users.remove(user)
        user['grads'].append(g)
        all_users.insert(index, user)
        UserControl.realoded_users(all_users)

